package com.ad2pro.spectra.common.producer;

import com.ad2pro.spectra.common.exception.MessageProducerException;
import com.ad2pro.spectra.common.redisclient.RedisApplicationContext;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class ProducerThread implements Runnable {

    /*@Autowired
    private RedisApplicationContext localApplicationContext;*/

    private CommonProducer commonProducer = (CommonProducer)
            RedisApplicationContext.getApplicationContext().getBean(CommonProducer.class);

    @Override
    public void run() {
        int i = 0;
        while(true) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("message", i++);
            try {
                commonProducer.produceMessage("commonQueue", jsonObject);
            } catch (MessageProducerException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
                break;
            }

        }
    }
}


