package com.ad2pro.spectra.common.producer;

import com.ad2pro.spectra.common.exception.MessageProducerException;
import com.ad2pro.spectra.common.config.RedisConfiguration;
import com.ad2pro.spectra.common.exception.QueueCreationException;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@Component
public class CommonProducer {

    public void produceMessage(String qName, JSONObject jsonObject) throws MessageProducerException {
        try {
            RedissonClient redisson = RedisConfiguration.getRedisClient();
            if(! redisson.getBlockingQueue(qName).isExists())
                throw new MessageProducerException("Queue does not exists. Please create a new queue");
            RBlockingQueue<JSONObject> queue = redisson.getBlockingQueue(qName);
            queue.offer(jsonObject);
            log.debug("Successfully enqueued message {} to the queue {}", jsonObject.toJSONString(), qName);
        }catch (Exception e){
            log.error("There was an error while pushing message {} into queue {}", jsonObject.toJSONString(), qName, e);
            throw new MessageProducerException(e.getMessage());
        }
    }


    public void createNewQueue(String qName) throws QueueCreationException {
        try{
            RedissonClient redisson = RedisConfiguration.getRedisClient();
            RBlockingQueue<JSONObject> queue = redisson.getBlockingQueue(qName);
        } catch (Exception e){
            log.error("Error while creating a new queue with the name {}", qName, e);
            throw new QueueCreationException(e.getMessage());
        }
    }
}
