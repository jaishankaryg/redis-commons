package com.ad2pro.spectra.common.service;

import com.ad2pro.spectra.common.exception.CacheReadException;
import com.ad2pro.spectra.common.exception.CacheUpdateException;
import com.ad2pro.spectra.common.config.RedisConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMap;
import org.springframework.stereotype.Service;

import java.util.concurrent.locks.ReadWriteLock;

@Service
@Slf4j
public class ReadWriteCacheService {

    public void writeToMap(String mapName, String key, String value) throws CacheUpdateException {
        RMap<String, String> map = RedisConfiguration.getRedisClient().getMap(mapName);
        ReadWriteLock rwLock = RedisConfiguration.getRedisClient().getReadWriteLock(mapName);
        try {
            rwLock.writeLock().lock();
            map.fastPut(key, value);
        }catch (Exception e){
            log.error("Error while writing into redis cache", e);
            throw new CacheUpdateException(e.getMessage());
        }finally {
            rwLock.writeLock().unlock();
        }

    }


    public String readFromMap(String mapName, String key) throws CacheReadException {
        RMap<String, String> map = RedisConfiguration.getRedisClient().getMap(mapName);
        ReadWriteLock rwLock = RedisConfiguration.getRedisClient().getReadWriteLock(mapName);
        try {
            rwLock.readLock().lock();
            return map.get(key);
        }catch (Exception e){
            log.error("Error while writing into redis cache", e);
            throw new CacheReadException(e.getMessage());
        }finally {
            rwLock.readLock().unlock();
        }
    }
}
