package com.ad2pro.spectra.common.controller;

import com.ad2pro.spectra.common.consumer.CommonConsumer;
import com.ad2pro.spectra.common.exception.*;
import com.ad2pro.spectra.common.producer.CommonProducer;
import com.ad2pro.spectra.common.service.ReadWriteCacheService;
import com.ad2pro.spectra.common.consumer.ConsumerThread;
import com.ad2pro.spectra.common.producer.ProducerThread;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class ReadWriteCacheController {

    @Autowired
    CommonProducer commonProducer;

    @Autowired
    CommonConsumer commonConsumer;

    @Autowired
    private ReadWriteCacheService readWriteCacheService;

    @RequestMapping(method = POST, value = "/cache/add")
    public void updateCache(@RequestBody JSONObject requestObj){
        String mapName = (String) requestObj.get("mapName");
        String key = (String) requestObj.get("key");
        String value = (String) requestObj.get("value");

        try {
            readWriteCacheService.writeToMap(mapName, key, value);
        } catch (CacheUpdateException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(method = POST, value = "/cache/read")
    public String readFromCache(@RequestBody JSONObject requestObj){
        String mapName = (String) requestObj.get("mapName");
        String key = (String) requestObj.get("key");

        try {
           return readWriteCacheService.readFromMap(mapName, key);
        } catch (CacheReadException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = POST, value = "/queue/qName/createa")
    public void createNewQueue(@PathVariable String qName){
        try {
            commonProducer.createNewQueue(qName);
        } catch (QueueCreationException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(method = POST, value = "/queue/{qName}/push")
    public void pushNewMessage(@RequestBody JSONObject messageObj, @PathVariable String qName){
        /*Thread producerThread = new Thread(new ProducerThread());
        producerThread.start();*/
        try {
            commonProducer.produceMessage(qName, messageObj);
        } catch (MessageProducerException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(method = GET, value = "/queue/{qName}/pop")
    public void popMessageFromQueue(@PathVariable String qName) {
        /*Thread consumerThread = new Thread(new ConsumerThread());
        consumerThread.start();*/
        try {
            commonConsumer.readMessage(qName);
        } catch (MessageConsumerException e) {
            e.printStackTrace();
        }

    }

}
