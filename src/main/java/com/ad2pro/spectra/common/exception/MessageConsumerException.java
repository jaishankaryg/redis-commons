package com.ad2pro.spectra.common.exception;

public class MessageConsumerException extends Exception {

    public MessageConsumerException(String message) {
        super(message);
    }
}
