package com.ad2pro.spectra.common.exception;

public class CacheReadException extends Exception{

    public CacheReadException(String message){
        super(message);
    }
}
