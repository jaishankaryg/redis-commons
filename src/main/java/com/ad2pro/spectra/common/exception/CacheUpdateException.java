package com.ad2pro.spectra.common.exception;

public class CacheUpdateException extends Exception {

    public CacheUpdateException(String message){
        super(message);
    }

}
