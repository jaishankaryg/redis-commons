package com.ad2pro.spectra.common.exception;

public class MessageProducerException extends Exception {

    public MessageProducerException(String message){
        super(message);
    }
}
