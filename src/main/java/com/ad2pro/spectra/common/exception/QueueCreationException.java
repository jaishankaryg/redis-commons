package com.ad2pro.spectra.common.exception;

public class QueueCreationException extends Exception {

    public QueueCreationException(String message){
        super(message);
    }
}
