package com.ad2pro.spectra.common.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

@Component
public class RedisConfiguration {

    private static RedissonClient client = null;

    //TODO - use sentinal configurations for cluster setup
    public RedisConfiguration() throws IOException {
        /*Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://127.0.0.1:6379");*/

        Config config = Config.fromJSON(
                new File("/Users/jaishankar/build-code/redis-client/src/main/resources/redis-cluster-config.json"));
        if(null == client)
            client = Redisson.create(config);
    }

    public static RedissonClient getRedisClient(){
        return client;
    }

}

