package com.ad2pro.spectra.common.consumer;

import com.ad2pro.spectra.common.redisclient.RedisApplicationContext;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ConsumerThread implements Runnable {

    private CommonConsumer commonConsumer = (CommonConsumer)
            RedisApplicationContext.getApplicationContext().getBean(CommonConsumer.class);

    @Override
    public void run() {

        while(true){
            try{
                Thread.sleep(2000);
                JSONObject readMessage = commonConsumer.readMessage("commonQueue");
                System.out.println("***********" + readMessage.toJSONString() + "***************" + Thread.currentThread().getId());
            }catch (Exception e){
                log.error("Error while reading from commonQueue");
            }
        }

    }
}
