package com.ad2pro.spectra.common.consumer;

import com.ad2pro.spectra.common.exception.MessageConsumerException;
import com.ad2pro.spectra.common.config.RedisConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.redisson.api.RBlockingQueue;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CommonConsumer {

    public JSONObject readMessage(String qName) throws MessageConsumerException {
        try {
            RedissonClient redisson = RedisConfiguration.getRedisClient();
            if(! redisson.getBlockingQueue(qName).isExists())
                throw new MessageConsumerException("There is no queue present with the name " + qName);
            RBlockingQueue<JSONObject> queue = redisson.getBlockingQueue(qName);
            return queue.poll();
        }catch (Exception e){
            log.error("Error while reading messages from queue {}", qName);
            throw new MessageConsumerException(e.getMessage());
        }
    }

}
