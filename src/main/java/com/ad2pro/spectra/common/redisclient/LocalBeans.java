package com.ad2pro.spectra.common.redisclient;

import com.ad2pro.spectra.common.consumer.CommonConsumer;
import com.ad2pro.spectra.common.producer.CommonProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LocalBeans {

    @Bean
    public CommonProducer commonProducer(){
        return new CommonProducer();
    }

    @Bean
    public CommonConsumer commonConsumer(){
        return new CommonConsumer();
    }

}
