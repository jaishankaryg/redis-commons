package com.ad2pro.spectra.common.redisclient;

import com.ad2pro.spectra.common.producer.CommonProducer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
@ComponentScan({"com.ad2pro.spectra.common.service", "com.ad2pro.spectra.common.controller",
		"com.ad2pro.spectra.common.redisclient", "com.ad2pro.spectra.common.config"})
public class RedisClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(RedisClientApplication.class, args);
	}

}
